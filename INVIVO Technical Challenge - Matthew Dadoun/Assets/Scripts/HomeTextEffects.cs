using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

//This class is heavily based on the Vertex Color Cycler script, included in TMPro Extras and Examples included with TextMeshPro from the Package Manager
public class HomeTextEffects : MonoBehaviour
{
    //the gradient it is using and the speed it changes between colors in the gradient
    public Gradient textGradient;
    public float gradientSpeed;


    private TMP_Text m_TextComponent;
    private float _totalTime;

    void Awake()
    {
        m_TextComponent = GetComponent<TMP_Text>();
    }

    TMP_TextInfo textInfo;
    int currentCharacter;

    Color32[] newVertexColors;
    Color32 c0;

    void Start()
    {
        // Force the text object to update right away so we can have geometry to modify right from the start.
        m_TextComponent.ForceMeshUpdate();

        //initialization of member variables
        textInfo = m_TextComponent.textInfo;
        currentCharacter = 0;
        Color32 c0 = m_TextComponent.color;
    }

    void Update()
    {
        //the amount of characters in the text used
        int characterCount = textInfo.characterCount;

        // Get the index of the material used by the current character.
        int materialIndex = textInfo.characterInfo[currentCharacter].materialReferenceIndex;

        // Get the vertex colors of the mesh used by this text element (character or sprite).
        newVertexColors = textInfo.meshInfo[materialIndex].colors32;

        // Get the index of the first vertex used by this text element.
        int vertexIndex = textInfo.characterInfo[currentCharacter].vertexIndex;

        // Only change the vertex color if the text element is visible.
        if (textInfo.characterInfo[currentCharacter].isVisible)
        {
            float offset = (currentCharacter / characterCount);
            c0 = textGradient.Evaluate((_totalTime + offset) % 1);
            _totalTime += Time.deltaTime*gradientSpeed;

            newVertexColors[vertexIndex + 0] = c0;
            newVertexColors[vertexIndex + 1] = c0;
            newVertexColors[vertexIndex + 2] = c0;
            newVertexColors[vertexIndex + 3] = c0;

            // New function which pushes (all) updated vertex data to the appropriate meshes when using either the Mesh Renderer or CanvasRenderer.
            m_TextComponent.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);

            // This last process could be done to only update the vertex data that has changed as opposed to all of the vertex data but it would require extra steps and knowing what type of renderer is used.
            // These extra steps would be a performance optimization but it is unlikely that such optimization will be necessary.
        }

        for (int i = 0; i < textInfo.characterCount; i++)
        {
            var characterInfo = textInfo.characterInfo[i];
            if (!characterInfo.isVisible)
            {
                continue;
            }

            var verts = textInfo.meshInfo[characterInfo.materialReferenceIndex].vertices;
            for (int j = 0; j < 4; j++)
            {
                var origin = verts[characterInfo.vertexIndex + j];
                verts[characterInfo.vertexIndex + j] = origin + new Vector3(0, Mathf.Sin(_totalTime * 8f + origin.x*0.01f) * 0.1f, 0);
            }
        }

        for (int i = 0; i < textInfo.meshInfo.Length; i++)
        {
            var meshInfo = textInfo.meshInfo[i];
            meshInfo.mesh.vertices = meshInfo.vertices;
        }
    currentCharacter = (currentCharacter + 1) % characterCount;
    }
}
