using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class SwitchBetweenObjects : MonoBehaviour
{
    // Start is called before the first frame update
    public TextMeshProUGUI statusText; 
    List<GameObject> activatableGameObjects;
    GameObject hitObject;
    GameObject previouslyHitObject;
    Renderer rend;
    Color opaque;
    void Start()
    {
        activatableGameObjects = new List<GameObject>();
        foreach (Transform child in transform)
        {
            activatableGameObjects.Add(child.gameObject);
        }
        opaque.a = 255;

    }

    // Update is called once per frame
    void Update()
    {
        CheckForClick();
    }

    private void CheckForClick()
    {

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        {
            if (Physics.Raycast(ray, out hitInfo) && Input.GetMouseButtonDown(0))
            {
                hitObject = GameObject.Find(hitInfo.collider.name);
                statusText.text = hitInfo.collider.name;

                if (previouslyHitObject == null)
                {
                    previouslyHitObject = hitObject;
                }

                var hitMaterial = hitObject.GetComponent<MeshRenderer>().material;
                Color newCol;

                if (hitObject != previouslyHitObject)
                {
                    newCol = previouslyHitObject.GetComponent<MeshRenderer>().material.color;
                    newCol.a = 0.23f;
                    previouslyHitObject.GetComponent<MeshRenderer>().material.color = newCol;
                }
                newCol = hitMaterial.color;
                previouslyHitObject = hitObject;
                newCol.a = 1;
                hitMaterial.color = newCol;
            }
        }
    }

}
