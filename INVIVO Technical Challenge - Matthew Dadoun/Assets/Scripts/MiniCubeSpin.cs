using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MiniCubeSpin : MonoBehaviour
{
    bool cubeHit = false;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100, 1))
            {
                if (hit.collider.gameObject == this.gameObject)
                {
                    cubeHit = !cubeHit;
                }
            }
        }

        if (cubeHit)
        {
            this.gameObject.transform.Rotate(1500 * Time.deltaTime, 0, 0);
        }
    }
}
