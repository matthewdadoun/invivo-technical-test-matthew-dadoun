using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterCubeMatLerp : MonoBehaviour
{
    Renderer rend;
    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        float lerp = Mathf.PingPong(Time.time, 3.0f) / 3.0f;
        rend.material.color = Color.Lerp(Color.red, Color.blue, lerp);
    }
}
