using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitch : MonoBehaviour
{
    public Animator fade;
    
    public void RunHomeRoutine()
    {
        StartCoroutine(GoHome());
    }

    public void RunNextScene()
    {
        StartCoroutine(NextScene(SceneManager.GetActiveScene().buildIndex));
    }

    public void RunLastScene()
    {
        StartCoroutine(LastScene(SceneManager.GetActiveScene().buildIndex));
    }

    IEnumerator GoHome()
    {
        fade.SetTrigger("StartFade");
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(0);
    }

    IEnumerator NextScene(int i)
    {
        fade.SetTrigger("StartFade");
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(i+1);
    }

    IEnumerator LastScene(int i)
    {
        fade.SetTrigger("StartFade");
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(i - 1);
    }
}
