using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class HomeCubeEffects : MonoBehaviour
{

    public Material cubeMat;
    public Animator fade;
    public float cubeScaleDecreaseAmount;
        
    Renderer rend;
    bool cubeHit=false; 
    Vector3 cubeScaleDecrease = new Vector3(8, 8, 8);

    Color32 cubeColor;

    // Start is called before the first frame update
    void Start()
    {
        //cubeMat = GetComponent<Material>();
        rend = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!cubeHit)
        {
            transform.Rotate(80*Time.deltaTime, 0, 32*Time.deltaTime);
        }
        float lerp = Mathf.PingPong(Time.time, 3.0f) / 3.0f;
        rend.material.color = Color.Lerp(Color.red, Color.blue, lerp);

        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100, 1))
            {
                //loads content scene 1 directly
                StartCoroutine(LoadContentScene());
                if (!cubeHit)
                {
                    cubeHit = !cubeHit;
                }
            }
        }

        if (cubeHit)
        {

            transform.Rotate(1500*Time.deltaTime, 0, 0);
            transform.localScale -= cubeScaleDecrease*Time.deltaTime;
        }
    }

    IEnumerator LoadContentScene()
    {
        fade.SetTrigger("StartFade");
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(1);
    }
}
